﻿using System;
using System.Windows.Forms;

namespace Library
{
    public partial class LossPanel : Form
    {
        private string bookname;
        private BookSql bookSql = new BookSql();
        private string sql = "";
        private Debug debug = new Debug();
        public LossPanel()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string[] bookinfos = bookSql.getBookInfo();
            this.bookname = textBox1.Text;
            int sign = bookSql.lossBook(this.bookname);
            if (sign == 0)
            {
                label1.Text = "本馆未收录该图书";
            }
            else if (sign == 1)
            {
                MessageBox.Show("挂失申请( " + bookinfos[2] + " )已提交，请前往前台办理赔偿手续");
            }
            else if (sign == -1)
            {
                MessageBox.Show("发现在馆图书失窃，已向安保处通报，感谢你的及时反馈");
            }
            else if (sign == -2)
            {
                MessageBox.Show("系统异常");
            }
            /*
            if (bookSql.queryBook(this.bookname))
            {
                string[] bookinfos = bookSql.getBookInfo();
                if (int.Parse(bookinfos[3]) > 0)
                {
                    if (int.Parse(bookinfos[4]) > 0)
                    {
                        MessageBox.Show("挂失申请( " + bookinfos[2] + " )已提交，请前往前台办理赔偿手续");
                    }
                    else
                    {
                        sql = "update bookinfo set bookname='" + textBox1.Text + "' where bookname='" + textBox1.Text + "';";
                        debug.console(sql);
                        debug.messageBox("console debug");
                        MessageBox.Show("已向安保处通报了，感谢你的及时反馈");
                    }
                }
                else
                {
                    label1.Text = "系统异常";
                }
            }
            else
            {
                label1.Text = "本馆未收录该图书";
            }
            */
        }
    }
}
