﻿
namespace Library
{
    partial class Book
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Book));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnF5 = new System.Windows.Forms.ToolStripButton();
            this.btnLend = new System.Windows.Forms.ToolStripButton();
            this.btnReturn = new System.Windows.Forms.ToolStripButton();
            this.btnLoss = new System.Windows.Forms.ToolStripButton();
            this.btnDonate = new System.Windows.Forms.ToolStripButton();
            this.btnDelay = new System.Windows.Forms.ToolStripButton();
            this.profile = new System.Windows.Forms.ToolStripSplitButton();
            this.btnChangeSecret = new System.Windows.Forms.ToolStripMenuItem();
            this.btnLogout = new System.Windows.Forms.ToolStripMenuItem();
            this.userid = new System.Windows.Forms.ToolStripLabel();
            this.btnPlay = new System.Windows.Forms.ToolStripButton();
            this.listView1 = new System.Windows.Forms.ListView();
            this.listBookNum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listBookName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listBookPrice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lisitBookNum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listBookNowNum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listBookLendNum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnF5,
            this.btnLend,
            this.btnReturn,
            this.btnLoss,
            this.btnDonate,
            this.btnDelay,
            this.profile,
            this.userid,
            this.btnPlay});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(820, 33);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnF5
            // 
            this.btnF5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnF5.Image = ((System.Drawing.Image)(resources.GetObject("btnF5.Image")));
            this.btnF5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnF5.Name = "btnF5";
            this.btnF5.Size = new System.Drawing.Size(50, 28);
            this.btnF5.Text = "刷新";
            this.btnF5.Click += new System.EventHandler(this.btnF5_Click);
            // 
            // btnLend
            // 
            this.btnLend.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnLend.Image = ((System.Drawing.Image)(resources.GetObject("btnLend.Image")));
            this.btnLend.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLend.Name = "btnLend";
            this.btnLend.Size = new System.Drawing.Size(86, 28);
            this.btnLend.Text = "借阅图书";
            this.btnLend.Click += new System.EventHandler(this.btnLend_Click);
            // 
            // btnReturn
            // 
            this.btnReturn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnReturn.Image = ((System.Drawing.Image)(resources.GetObject("btnReturn.Image")));
            this.btnReturn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(86, 28);
            this.btnReturn.Text = "归还图书";
            this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
            // 
            // btnLoss
            // 
            this.btnLoss.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnLoss.Image = ((System.Drawing.Image)(resources.GetObject("btnLoss.Image")));
            this.btnLoss.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLoss.Name = "btnLoss";
            this.btnLoss.Size = new System.Drawing.Size(86, 28);
            this.btnLoss.Text = "图书挂失";
            this.btnLoss.Click += new System.EventHandler(this.btnLoss_Click);
            // 
            // btnDonate
            // 
            this.btnDonate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnDonate.Image = ((System.Drawing.Image)(resources.GetObject("btnDonate.Image")));
            this.btnDonate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDonate.Name = "btnDonate";
            this.btnDonate.Size = new System.Drawing.Size(86, 28);
            this.btnDonate.Text = "图书捐赠";
            this.btnDonate.Click += new System.EventHandler(this.btnDonate_Click);
            // 
            // btnDelay
            // 
            this.btnDelay.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnDelay.Image = ((System.Drawing.Image)(resources.GetObject("btnDelay.Image")));
            this.btnDelay.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelay.Name = "btnDelay";
            this.btnDelay.Size = new System.Drawing.Size(86, 28);
            this.btnDelay.Text = "图书延期";
            this.btnDelay.Click += new System.EventHandler(this.btnDelay_Click);
            // 
            // profile
            // 
            this.profile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.profile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnChangeSecret,
            this.btnLogout});
            this.profile.Image = ((System.Drawing.Image)(resources.GetObject("profile.Image")));
            this.profile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.profile.Name = "profile";
            this.profile.Size = new System.Drawing.Size(103, 28);
            this.profile.Text = "个人中心";
            // 
            // btnChangeSecret
            // 
            this.btnChangeSecret.Name = "btnChangeSecret";
            this.btnChangeSecret.Size = new System.Drawing.Size(182, 34);
            this.btnChangeSecret.Text = "修改密码";
            this.btnChangeSecret.Click += new System.EventHandler(this.btnChangeSecret_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(182, 34);
            this.btnLogout.Text = "退出登录";
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // userid
            // 
            this.userid.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.userid.Name = "userid";
            this.userid.Size = new System.Drawing.Size(0, 28);
            // 
            // btnPlay
            // 
            this.btnPlay.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPlay.Image = ((System.Drawing.Image)(resources.GetObject("btnPlay.Image")));
            this.btnPlay.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(34, 28);
            this.btnPlay.Click += new System.EventHandler(this.playmusic);
            // 
            // listView1
            // 
            this.listView1.BackgroundImageTiled = true;
            this.listView1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.listBookNum,
            this.listBookName,
            this.listBookPrice,
            this.lisitBookNum,
            this.listBookNowNum,
            this.listBookLendNum});
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(0, 33);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(820, 417);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // listBookNum
            // 
            this.listBookNum.Text = "图书编号";
            this.listBookNum.Width = 84;
            // 
            // listBookName
            // 
            this.listBookName.Text = "图书名字";
            this.listBookName.Width = 100;
            // 
            // listBookPrice
            // 
            this.listBookPrice.Text = "图书价格";
            this.listBookPrice.Width = 84;
            // 
            // lisitBookNum
            // 
            this.lisitBookNum.Text = "图书总量";
            this.lisitBookNum.Width = 84;
            // 
            // listBookNowNum
            // 
            this.listBookNowNum.Text = "在馆数量";
            this.listBookNowNum.Width = 84;
            // 
            // listBookLendNum
            // 
            this.listBookLendNum.Text = "借出数量";
            this.listBookLendNum.Width = 84;
            // 
            // Book
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(820, 450);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.toolStrip1);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Book";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "欢迎来到蒙德图书馆~ 主页面";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.form_closed);
            this.Load += new System.EventHandler(this.form_load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnF5;
        private System.Windows.Forms.ToolStripButton btnLend;
        private System.Windows.Forms.ToolStripButton btnReturn;
        private System.Windows.Forms.ToolStripButton btnDonate;
        private System.Windows.Forms.ToolStripButton btnLoss;
        private System.Windows.Forms.ToolStripButton btnDelay;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader listBookNum;
        private System.Windows.Forms.ColumnHeader listBookName;
        private System.Windows.Forms.ColumnHeader listBookPrice;
        private System.Windows.Forms.ColumnHeader lisitBookNum;
        private System.Windows.Forms.ColumnHeader listBookNowNum;
        private System.Windows.Forms.ColumnHeader listBookLendNum;
        private System.Windows.Forms.ToolStripSplitButton profile;
        private System.Windows.Forms.ToolStripMenuItem btnChangeSecret;
        private System.Windows.Forms.ToolStripMenuItem btnLogout;
        private System.Windows.Forms.ToolStripLabel userid;
        private System.Windows.Forms.ToolStripButton btnPlay;
    }
}