﻿using System.Collections.Generic;

namespace Library
{
    class UserSession
    {
        private string uid;
        private string upwd;

        public void setUserinfo(string id, string pwd) { this.uid = id; this.upwd = pwd; }

        public Queue<string> getInfo()
        {
            Queue<string> queue = new Queue<string>();
            queue.Enqueue(this.uid);
            queue.Enqueue(this.upwd);
            return queue;
        }
    }
}
