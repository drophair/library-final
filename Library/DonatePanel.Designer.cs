﻿
namespace Library
{
    partial class DonatePanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.bookname = new System.Windows.Forms.TextBox();
            this.booknums = new System.Windows.Forms.TextBox();
            this.btnDonate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(85, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "书名：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(85, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "数量：";
            // 
            // bookname
            // 
            this.bookname.Location = new System.Drawing.Point(165, 69);
            this.bookname.Name = "bookname";
            this.bookname.Size = new System.Drawing.Size(130, 28);
            this.bookname.TabIndex = 2;
            // 
            // booknums
            // 
            this.booknums.Location = new System.Drawing.Point(164, 125);
            this.booknums.Name = "booknums";
            this.booknums.Size = new System.Drawing.Size(130, 28);
            this.booknums.TabIndex = 3;
            // 
            // btnDonate
            // 
            this.btnDonate.Location = new System.Drawing.Point(333, 69);
            this.btnDonate.Name = "btnDonate";
            this.btnDonate.Size = new System.Drawing.Size(75, 79);
            this.btnDonate.TabIndex = 4;
            this.btnDonate.Text = "捐赠";
            this.btnDonate.UseVisualStyleBackColor = true;
            this.btnDonate.Click += new System.EventHandler(this.btnDonate_Click);
            // 
            // DonatePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 215);
            this.Controls.Add(this.btnDonate);
            this.Controls.Add(this.booknums);
            this.Controls.Add(this.bookname);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "DonatePanel";
            this.Text = "图书捐赠";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox bookname;
        private System.Windows.Forms.TextBox booknums;
        private System.Windows.Forms.Button btnDonate;
    }
}