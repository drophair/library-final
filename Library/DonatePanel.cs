﻿using System.Windows.Forms;

namespace Library
{
    public partial class DonatePanel : Form
    {
        private BookSql bookSql = new BookSql();
        private Debug debug = new Debug();
        public DonatePanel()
        {
            InitializeComponent();
        }

        private void btnDonate_Click(object sender, System.EventArgs e)
        {
            string[] bookinfos = bookSql.getBookInfo();
            debug.console(bookname.Text);
            bookSql.addBook(bookname.Text, int.Parse(booknums.Text));
            MessageBox.Show("感谢你为图书馆做出无量贡献");
        }
    }
}
