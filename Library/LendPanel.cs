﻿using System.Windows.Forms;

namespace Library
{
    public partial class LendPanel : Form
    {
        public LendPanel()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            BookSql sql = new BookSql();
            int sign = sql.changeBook(textBox1.Text, 1);
            bool flag = sql.queryBook(this.textBox1.Text);
            if (sign == 2)
            {
                MessageBox.Show("借阅成功", "提示");
                this.Hide();
            }
            else
            {
                if (flag)
                    this.label1.Text = "借阅火爆，稍后再来借吧";
                else
                    this.label1.Text = "借阅失败，该书不是本馆注册图书";
            }
        }
    }
}
