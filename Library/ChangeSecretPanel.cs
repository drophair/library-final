﻿using System;
using System.Windows.Forms;

namespace Library
{
    public partial class ChangeSecretPanel : Form
    {
        Debug debug = new Debug();
        private string uid, upwd;
        private string pwd;
        public ChangeSecretPanel()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            pwd = this.textBox1.Text;
            bool sign = false;
            if (pwd.Equals(this.upwd))
            {
                debug.messageBox("即将设置的密码不得与当前密码相同");
            }
            else if (pwd.Equals(""))
            {
                debug.messageBox("设置密码不得为空");
            }
            else
            {
                UserSql userSql = new UserSql();
                userSql.setInfo(this.uid, this.upwd);
                userSql.changePwd(this.uid, this.pwd);
                sign = true;
                debug.messageBox("密码设置成功,返回请退出重新登录");

                //子窗体和父窗体同时关闭
                base.Close();
                Application.Exit();
                //this.Close();
                //System.Environment.Exit(0);
            }
            //return sign;
        }

        public void setInfo(string id, string pwd)
        {
            this.uid = id;
            this.upwd = pwd;
        }
    }
}
