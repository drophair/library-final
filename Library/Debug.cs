﻿using System;
using System.Windows.Forms;

namespace Library
{
    class Debug
    {
        public static void messageBox_static(string str, string title)
        {
            MessageBox.Show(str, title);
        }
        public void messageBox(string str)
        {
            MessageBox.Show(str);
        }
        public static void console_static(string str)
        {
            Console.WriteLine(str);
        }
        public void console(string str)
        {
            Console.WriteLine(str);
        }
    }
}
