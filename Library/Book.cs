﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Drawing;
using System.Media;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
//using System.Drawing;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library
{
    public partial class Book : Form
    {
        private bool musicSign;
        //private string musicTimer;
        private string uid, upwd;
        //private string url = "server=192.168.99.57;port=3306;user=root;password=123456;database=book;Charset=utf8;SslMode=None;";
        //private string url = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + "D:\\accessdb\\userinfo.mdb";
        //private string url = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=userinfo.mdb";
        private string url = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=..\\..\\src\\db\\userinfo.mdb";
        private string sql = "";
        private OleDbDataReader reader = null;
        private SoundPlayer soundPlayer;
        public Book()
        {
            InitializeComponent();
        }

        public void setInfo(string id, string pwd)
        {
            this.uid = id;
            this.upwd = pwd;
        }

        public Queue<string> getInfo()
        {
            Queue<string> info = new Queue<string>();
            info.Enqueue(this.uid);
            info.Enqueue(this.upwd);
            return info;
        }
        public void f5()
        {
            listView1.Items.Clear();
            sql = "select * from bookinfo;";
            OleDbConnection oleDbConnection = new OleDbConnection(url);
            try
            {
                oleDbConnection.Open();
                OleDbCommand mySqlCommand = new OleDbCommand(sql, oleDbConnection);
                reader = mySqlCommand.ExecuteReader();
                //int listintem = 0;
                while (reader.Read())
                {
                    int bookid, booknum, booknownum, booklendnum, bookleftnum;
                    string bookname, bookprice;
                    bookid = reader.GetInt32(0);
                    bookname = reader.GetString(1);
                    bookprice = reader.GetString(2);
                    booknum = reader.GetInt32(3);
                    booknownum = reader.GetInt32(4);
                    booklendnum = reader.GetInt32(5);
                    bookleftnum = reader.GetInt32(6);
                    //MessageBox.Show(bookname.ToString());
                    Console.WriteLine(bookid + " " + bookname + " " + bookprice + " " + booknum + " " + booknownum + " " + booklendnum + " " + bookleftnum);

                    string[] tempStr = new string[] { bookid.ToString(), bookname, bookprice, booknum.ToString(), booknownum.ToString(), booklendnum.ToString() };
                    ListViewItem listViewIterm = new ListViewItem(tempStr);
                    listView1.Items.Add(listViewIterm);

                    //listViewIterm.Index = listintem++;
                }
                oleDbConnection.Close();
            }
            catch (OleDbException ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("数据库连接失败,请及时联系图书馆管理员或至图书馆前台人工办理");
                oleDbConnection.Close();
            }
            finally
            {
                oleDbConnection.Close();
            }
        }

        private void btnF5_Click(object sender, EventArgs e)
        {
            f5();
        }

        private void btnLend_Click(object sender, EventArgs e)
        {
            //CommonClass commonClass = new CommonClass();
            //CommonPanel commonPanel = new CommonPanel();
            //commonPanel.Show();
            //commonClass.setLabelName("借阅书名：");
            LendPanel lendPanel = new LendPanel();
            lendPanel.Show();

        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            ReturnPanel returnPanel = new ReturnPanel();
            returnPanel.Show();
        }

        private void btnLoss_Click(object sender, EventArgs e)
        {
            LossPanel lossPanel = new LossPanel();
            lossPanel.Show();
        }

        private void btnDonate_Click(object sender, EventArgs e)
        {
            DonatePanel donatePanel = new DonatePanel();
            donatePanel.Show();
        }

        private void btnDelay_Click(object sender, EventArgs e)
        {
            DelayPanel delayPanel = new DelayPanel();
            delayPanel.Show();
        }

        private void btnChangeSecret_Click(object sender, EventArgs e)
        {
            ChangeSecretPanel changeSecretPanel = new ChangeSecretPanel();
            changeSecretPanel.setInfo(this.uid, this.upwd);
            changeSecretPanel.Show();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            //this.Close();
            //关闭窗体和所有线程
            System.Environment.Exit(0);
        }

        private void playmusic(object sender, EventArgs e)
        {
            if (this.musicSign == true)
            {
                //this.musicTimer=soundPlayer.Ctlcontrols.currentPositionString;
                soundPlayer.Stop();
                this.btnPlay.Image = Image.FromFile(@"..\..\src\img\play.png");
                this.btnPlay.Text = "暂停";
                musicSign = false;
            }
            else
            {
                soundPlayer.Play();
                //Console.WriteLine(Directory.GetCurrentDirectory());
                this.btnPlay.Image = Image.FromFile(@"..\..\src\img\stop.png");
                this.btnPlay.Text = "播放";
                musicSign = true;
            }
        }

        private void form_closed(object sender, FormClosedEventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void form_load(object sender, EventArgs e)
        {
            this.musicSign = true;
            f5();
            soundPlayer = new SoundPlayer(@"..\..\src\mp3\mengde.wav");
            soundPlayer.PlayLooping();
            this.btnPlay.Text = "播放";

            this.userid.Text = this.uid + " 同学，你好";
            //this.userid.Text = "sagjh";
        }
    }
}
