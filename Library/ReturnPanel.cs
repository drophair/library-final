﻿using System.Windows.Forms;

namespace Library
{
    public partial class ReturnPanel : Form
    {
        private BookSql bookSql = new BookSql();
        private string[] bookinfo;
        private Debug debug = new Debug();
        public ReturnPanel()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            //bookinfo = bookSql.getBookInfo();
            if (bookSql.queryBook(bookname.Text))
            {
                int sign = bookSql.changeBook(bookname.Text, 2);
                if (sign == 3)
                    debug.messageBox("图书归还成功");
                else if (sign == -3)
                    debug.messageBox("该书在本馆尚无借出记录");
                //else
                //{
                //    //debug.messageBox("toString 的 结果为：" + bookSql.tostring()+"----"+sign);
                //    //debug.console(bookinfo[3]);
                //    //debug.console(bookinfo[5]);
                //    //debug.messageBox("1捐赠图书，请走捐赠通道，谢谢");
                //}
            }
            else
            {
                debug.messageBox("本馆尚未收录该书");
            }
        }
    }
}
