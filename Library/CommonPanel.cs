﻿using System;
using System.Windows.Forms;

namespace Library
{
    public partial class CommonPanel : Form
    {
        public CommonPanel()
        {
            InitializeComponent();
        }

        private void form_load(object sender, EventArgs e)
        {
            CommonClass commonClass = new CommonClass();
            this.label1.Text = commonClass.getLabelName();
            this.button1.Text = commonClass.getButtonName();
            int flag = commonClass.setMode();
        }
    }
}
