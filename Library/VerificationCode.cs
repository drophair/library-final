﻿using System;
using System.Drawing;

namespace Library
{
    class VerificationCode
    {
        private string pi = "31415926535897933141592653589793";
        private string nums = "012345678901234567890123456789";
        //private string charB = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private string charL = "abcdefghijklmnopqrstuvwxyz";
        private Random rd = new Random();

        private string str = "";

        private void initCode()
        {
            for (int i = 0; i < 4; i++)
            {
                int idx = rd.Next();
                int status = rd.Next();
                idx %= 26;
                status %= 2;
                if (status == 0)
                {
                    this.str += nums[idx];
                }
                else if (status == 1)
                {
                    this.str += charL[idx];
                }
                else if (status == 2)
                {
                    this.str += charL[idx];
                }
            }
        }

        public Bitmap generatePic()
        {
            this.str = null;
            initCode();
            Bitmap bitmap = new Bitmap(100, 50);
            Graphics g = Graphics.FromImage(bitmap);
            for (int i = 0; i < 4; i++)
            {
                String[] fonts = { "微软雅黑", "宋体", "黑体", "隶书", "仿宋" };//随机设置字体的样式
                Point p = new Point(i * 15, 0);
                Color[] colors = { Color.Red, Color.Pink, Color.Black, Color.Gray, Color.GreenYellow };//随机设置字体的颜色
                g.DrawString(str[i].ToString(), new Font(fonts[rd.Next(0, 5)], 20, FontStyle.Bold), new SolidBrush(colors[rd.Next(0, 5)]), p);//画图

            }
            for (int i = 0; i < 10; i++)//画出声燥线

            {
                Point p1 = new Point(rd.Next(0, bitmap.Width), rd.Next(0, bitmap.Height));
                Point p2 = new Point(rd.Next(0, bitmap.Width), rd.Next(0, bitmap.Height));
                g.DrawLine(new Pen(Brushes.Green), p1, p2);
            }
            return bitmap;
        }

        public bool checkCode(string code)
        {
            Debug debug = new Debug();
            debug.console("---" + code);
            bool sign = false;
            if (code.Equals(this.str) == true) { sign = true; }
            return sign;
        }
    }
}
