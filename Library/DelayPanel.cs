﻿using System.Windows.Forms;

namespace Library
{
    public partial class DelayPanel : Form
    {
        private BookSql bookSql = new BookSql();
        private Debug debug = new Debug();
        private string bookname;
        public DelayPanel()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            this.bookname = textBox1.Text;
            if (bookSql.queryBook(this.bookname))
            {
                debug.messageBox("延期成功");
                this.Dispose();
            }
            else
            {
                debug.messageBox("本馆未收录该图书");
            }
        }
    }
}
