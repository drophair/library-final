﻿using System;
using System.ComponentModel;
using System.Media;
using System.Threading;
using System.Windows.Forms;



namespace Library
{
    public partial class WelcomePanel : Form
    {
        public WelcomePanel()
        {
            InitializeComponent();

            //BackgroundWorker worker;
            //worker = new BackgroundWorker();
        }

        private void loadingForm(object sender, EventArgs e)
        {
            this.pictureBox1.ImageLocation = @"..\..\src\img\welcome.png";
            SoundPlayer soundPlayer = new SoundPlayer(@"..\..\src\mp3\first-meet.wav");
            soundPlayer.Play();
            using (BackgroundWorker bw = new BackgroundWorker())
            {
                bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);//开始的事件

                bw.DoWork += new DoWorkEventHandler(bw_DoWork);//完成的事件

                bw.RunWorkerAsync("Tank");
            }
            //Console.WriteLine("当前进度条数字是：" + loadingBar.Value);
            ////MessageBox.Show("OK");
            //loadingBar.Value = 0;
            //for(int i = 0; i < 100; i++)
            //{
            //    loadingBar.Value += 1;
            //    Console.WriteLine("当前进度条数字是：" + loadingBar.Value);
            //    Thread.Sleep(100);
            //}
        }
        void bw_DoWork(object sender, DoWorkEventArgs e)// 这里是后台线程
        {
            CheckForIllegalCrossThreadCalls = false;
            this.loadingBar.Value = 0;
            int i = 0;
            for (; i <= 100; i++)
            {
                if (i < 95)
                    Thread.Sleep(95);//延时
                else if (i <= 98)
                    Thread.Sleep(200);
                else
                    Thread.Sleep(500);

                this.percentLoading.Text = i.ToString() + "%";
                this.loadingBar.Value = i;


            }
            this.percentLoading.Text = (i - 1).ToString() + "%";
            Thread.Sleep(2000);
        }

        void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)//后台线程完成后的响应事件
        {
            UserSql userSql = new UserSql();
            bool sign = userSql.isConnect();
            if (sign == true)
            {
                Debug.messageBox_static("数据库连接正常", "成功");
            }
            else
            {
                Debug.messageBox_static("数据库连接失败,你可能需要重新检查一下数据库存放的位置\n应该放在D盘下accessdb文件夹下内", "王波老师，你好");
                System.Environment.Exit(0);
            }
            this.Hide();
            Console.WriteLine("OK");
            LoginPanel mainPanel = new LoginPanel();
            mainPanel.Show();
            //WelcomeLoading.main = new Main();

            //C_Global_variable.main.Show();
        }

        private void percentLoading_Click(object sender, EventArgs e)
        {

        }
    }
}
