﻿using System.Collections.Generic;
using System.Data.OleDb;

namespace Library
{
    class UserSql
    {
        private string uid, upwd;
        //private string url = "server=192.168.99.57;port=3306;user=root;password=123456;database=book;Charset=utf8;SslMode=None;";
        //private string url = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + "D:\\accessdb\\userinfo.mdb";
        //private string url = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=userinfo.mdb";
        private string url = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=..\\..\\src\\db\\userinfo.mdb";
        private string sql = "";
        private OleDbDataReader reader = null;
        public void setInfo(string id, string pwd)
        {
            this.uid = id;
            this.upwd = pwd;
        }

        public bool isConnect()
        {
            bool sign = false;
            OleDbConnection oleDbConnection = new OleDbConnection(url);
            try
            {
                oleDbConnection.Open();
                oleDbConnection.Close();
                sign = true;
            }
            catch
            {
                oleDbConnection.Close();
            }
            return sign;
        }
        public Queue<string> getInfo()
        {
            Queue<string> queue = new Queue<string>();
            queue.Enqueue(this.uid);
            queue.Enqueue(this.upwd);
            return queue;
        }

        public int changePwd(string id, string pwd)
        {
            int sign = 0;
            string sql = "update userinfo set Passwd='" + pwd + "' where UserId='" + id + "';";
            Debug debug = new Debug();
            debug.console(sql);
            OleDbConnection mySqlConnection = new OleDbConnection(url);
            try
            {
                mySqlConnection.Open();
                OleDbCommand mySqlCommand = new OleDbCommand(sql, mySqlConnection);
                sign = mySqlCommand.ExecuteNonQuery();
                mySqlConnection.Close();
            }
            catch
            {
                mySqlConnection.Close();
            }
            finally
            {
                mySqlConnection.Close();
            }
            return sign;
        }
    }
}
