﻿//using OleDb.Data.OleDbClient;
using System;
using System.Data.OleDb;
using System.Windows.Forms;

namespace Library
{

    public partial class LoginPanel : Form
    {
        private string stuid;
        private string stupwd;
        private string code;
        //string url = "server=192.168.99.57;port=3306;user=root;password=123456;database=book;Charset=utf8;SslMode=None;";
        private string url = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + "..\\..\\src\\db\\userinfo.mdb";
        OleDbDataReader reader = null;
        private VerificationCode verificationCode = new VerificationCode();
        private Debug debug = new Debug();
        public LoginPanel()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (verificationCode.checkCode(codeText.Text) == true)
            {
                stuid = stuId.Text;
                stupwd = stuPwd.Text;
                UserSession userSession = new UserSession();
                userSession.setUserinfo(stuid, stupwd);
                Console.WriteLine(userSession.getInfo().ToString());
                //MessageBox.Show(stuid + ' ' + stupwd);
                OleDbConnection connection = new OleDbConnection(url);
                try
                {
                    connection.Open();
                    string sql = "select * from userinfo where UserId='" + stuid + "' and Passwd='" + stupwd + "';";
                    Console.WriteLine(sql);
                    //string sql = "insert into p value (" + stuid + "," + stupwd + ")";
                    //MessageBox.Show(sql);
                    OleDbCommand cmd = new OleDbCommand(sql, connection);
                    reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        //MessageBox.Show("登录成功");
                        Book book = new Book();
                        book.setInfo(this.stuid, this.stupwd);
                        book.Show();
                        this.Hide();
                    }
                    else
                    {
                        loginInfo.Text = "登陆失败,账号或密码错误";
                        //MessageBox.Show("登陆失败,账号或密码错误");
                    }
                    //cmd.ExecuteNonQuery();
                    //MessageBox.Show("插入成功", "信息");
                }
                catch (OleDbException ex)
                {
                    Console.WriteLine(ex);
                    MessageBox.Show("连接错误", "警告");
                    connection.Close();
                }
                finally
                {
                    connection.Close();
                }
                this.pictureBox1.Image = verificationCode.generatePic();
            }
            else
            {
                this.loginInfo.Text = "验证码错误";
                this.pictureBox1.Image = verificationCode.generatePic();
                debug.console("验证码是：" + this.code);
                //debug.messageBox("BUG");
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            code = this.codeText.Text;
            this.pictureBox1.Image = verificationCode.generatePic();
        }

        private void form_load(object sender, EventArgs e)
        {
            this.pictureBox1.Image = verificationCode.generatePic();
        }

        private void form_closing(object sender, FormClosingEventArgs e)
        {
            System.Environment.Exit(0);
        }
    }
}
