﻿namespace Library
{
    class CommonClass
    {
        private string flag = "";
        private string labelName = "";
        private string textBoxValue = "";
        private string buttonName = "";

        public void setFlag(string tmp)
        {
            this.flag = tmp;
        }
        public void setLabelName(string tmp)
        {
            this.labelName = tmp;
        }
        public void setTextBoxValue(string tmp)
        {
            this.textBoxValue = tmp;
        }
        public void setButtonName(string tmp)
        {
            this.buttonName = tmp;
        }

        public string getFlag()
        {
            return this.flag;
        }
        public string getLabelName()
        {
            return this.labelName;
        }
        public string getTextBoxValue()
        {
            return this.textBoxValue;
        }
        public string getButtonName()
        {
            return this.buttonName;
        }

        public int setMode()
        {
            int sign = 0;
            if (this.flag == "借阅")
                sign = 0;
            else if (this.flag == "归还")
                sign = 1;
            else if (this.flag == "挂失")
                sign = 2;
            else if (this.flag == "捐赠")
                sign = 3;
            else if (this.flag == "延期")
                sign = 4;
            else
                sign = -1;
            return sign;
        }
    }
}
